package keygen

import (
	"github.com/tyler-smith/go-bip39"
	"github.com/tyler-smith/go-bip39/wordlists"
	"unicode"
)

func Lang(str string) {
	for _, v := range str {
		if unicode.Is(unicode.Han, v) {
			bip39.SetWordList(wordlists.ChineseSimplified)
		} else if unicode.Is(unicode.Hiragana, v) {
			bip39.SetWordList(wordlists.Japanese)
		} else if unicode.Is(unicode.Hangul, v) {
			bip39.SetWordList(wordlists.Korean)
		} else {
			bip39.SetWordList(wordlists.English)
		}
		break
	}
}
