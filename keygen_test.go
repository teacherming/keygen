package keygen

import (
	"encoding/hex"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/tyler-smith/go-bip39"
)

func TestBip39Entry(t *testing.T) {
	expected := "spirit local then slender harvest wait govern inflict chef tail word cram"
	entropy, err := hex.DecodeString("d2106b80e5a697ecd9439b275babf599")
	assert.NoError(t, err)
	mnemonic, err := bip39.NewMnemonic(entropy)
	if err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, expected, mnemonic)
}

func TestBip39Mne(t *testing.T) {
	expected := "d2106b80e5a697ecd9439b275babf599"
	entropy, err := bip39.EntropyFromMnemonic("spirit local then slender harvest wait govern inflict chef tail word cram")
	if err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, expected, hex.EncodeToString(entropy))
	expectedSeed := "43c4d719d017f98290203da7627bb6bd49fee87cec48c61b38c8216d31a6e969198c222dc8c0f700dfaac129948438c7860cd1d6e0b76667f282c5f16372918f"
	seed := bip39.NewSeed("spirit local then slender harvest wait govern inflict chef tail word cram", "123")
	assert.Equal(t, expectedSeed, hex.EncodeToString(seed))
}

func TestGenBtc(t *testing.T) {
	data := []struct {
		BitSize     int
		Mnemonic    string
		Pass        string
		Seed        string
		CoinType    CoinType
		RootKey     string
		AccountPKey string
		Path        []string
		Address     []string
		PublicKey   []string
		PrivateKey  []string
	}{
		{
			BitSize:     160,
			Mnemonic:    "spirit local then slender harvest wait govern inflict chef tail word cram",
			Pass:        "123",
			CoinType:    CoinTypeBTC,
			Seed:        "43c4d719d017f98290203da7627bb6bd49fee87cec48c61b38c8216d31a6e969198c222dc8c0f700dfaac129948438c7860cd1d6e0b76667f282c5f16372918f",
			RootKey:     "xprv9s21ZrQH143K3XEBY5Jg3TFHdCbsjmh5bCADKT8f7WUqG6Rw7FFGSip1Tjti3PS81zggMLZqdNVi7H3qSLkiPns61bnWiMrX6RVypCRDCxv",
			AccountPKey: "xprv9yPwH7Bepbcaa8uXDWfyBYheuoNhPGuD7kid7RcsLbjkGBekpANGfK353ygxAU4jkkSBez8573A4Y8xJQZUyD3EHYquh6F7QniusYUHjzCq",
			Path: []string{
				"m/44'/0'/0'/0/0",
				"m/44'/0'/0'/0/1",
				"m/44'/0'/0'/0/2",
			},
			Address: []string{
				"1FLiPus9o8k6r6jhejjtb4zJeaZcPiTGDZ",
				"1QJ42GbEKnjF4JX3vWewR9d4EkhckTFtpY",
				"13oUgDjvKd1Bb1h4YtZtwXgfuGy9pBA4nk",
			},
			PublicKey: []string{
				"02e8a33a95be1baf91d0c9bef5fe02731a29e187341f95c1ee5c255c93131a1d93",
				"020f41c5918bb42ef1ebaf86ba11f1476eaea2dcf52b018ce398ec40a10c74d7cd",
				"03b3d63961e07c720251146d8779cd691fe221c90fe211a64a9f459f02776f7186",
			},
			PrivateKey: []string{
				"L42bmYM86927nbD6rzsqyfL6kqvTiD1az7DoxnLpYBp9NuknVp6u",
				"L5BwUpQBzVm2jBS4Ey2ExN4kV86KBvaDxw5EuuEvvrHco82SoqZh",
				"KxXLVAMmnw7v2sNqTssfVksBbgyHfD4TzoqsyKWyDk2KQiWV9n5f",
			},
		},
		{
			BitSize:     160,
			Mnemonic:    "culture purpose monster danger logic local pizza into embody pottery lift asset tone finish walk",
			Pass:        "123",
			CoinType:    CoinTypeTestnet,
			Seed:        "3dfe86c87142453b01b1f8469977043e5879e986753953aa1104ea9a761c29b963e48b41b536cefabe6a6227a4c341f16240a04453790670878f438db9a6eb9f",
			RootKey:     "tprv8ZgxMBicQKsPfM1ZsX6jjKwrK3qJaSKNVp9KkAbMegQZxCQsxAQHbyrGDjSaTuFP3MmxYTd85AECJf9AJCUrdXM1HkBtAknDkgwLKxF6gyb",
			AccountPKey: "tpubDCSU27CjFjxUfxLTrihKxaoDCmwEEnyzoipRapqMo2MbDtsicRr6fwAp5VnqQCz8QiDBhQH5QEAUEeRmNDaztJaEo1bagtxq57xHijT8gxX",
			Path: []string{
				"m/44'/1'/0'/0/0",
				"m/44'/1'/0'/0/1",
				"m/44'/1'/0'/0/2",
			},
			Address: []string{
				"mx1RFyFYV6rudrcSvSJumCbF9UWNXyRCYh",
				"mxBYH5c7NfhpVDaVU5mTqLijtxgzwdiqHJ",
				"myknFFFvSgBvHBKSYJ439uJYU1ki94NuxU",
			},
			PublicKey: []string{
				"0313eef5786336e41f3b6e156d802f04f983812c4a1cff4882f0bc6888ff3eae77",
				"02a48228f08d7c4dc78b89c9cdd171bffb8fa2284eadb30d7b1adfbbb6647dd7af",
				"03dfa4b69cf7cb82ebe63c0b6d3d13f7a4b93ff058df9c994ff921bb46418e6346",
			},
			PrivateKey: []string{
				"cMtYfPdjy4L9zow4fsxyqTUL5cqi9ayjghMPmcTXD1ENryFpQukG",
				"cT19e36PgF2KdJdQzbw9p623nB7JVaNoKvhQUY7fyHLv6f83xmm5",
				"cURXfzVHRtTSXpdgN2iQYGYhfXTrQNjVpRa7XruZwKVFQ9EWz4HC",
			},
		},
		{
			BitSize:     160,
			Mnemonic:    "spirit local then slender harvest wait govern inflict chef tail word cram",
			Pass:        "123",
			CoinType:    CoinTypeETH,
			Seed:        "43c4d719d017f98290203da7627bb6bd49fee87cec48c61b38c8216d31a6e969198c222dc8c0f700dfaac129948438c7860cd1d6e0b76667f282c5f16372918f",
			RootKey:     "xprv9s21ZrQH143K3XEBY5Jg3TFHdCbsjmh5bCADKT8f7WUqG6Rw7FFGSip1Tjti3PS81zggMLZqdNVi7H3qSLkiPns61bnWiMrX6RVypCRDCxv",
			AccountPKey: "xprv9zWTMmYJnRXYKSdmAZMWRMzNdS9k4fxQSH2mGuhN6CcE6gGscPKbLNyv88vubfSp6mEaJmNssmfe4QeStxjtVBp5DJ5M9sLW9GHV3NtAW4i",
			Path: []string{
				"m/44'/60'/0'/0/0",
				"m/44'/60'/0'/0/1",
				"m/44'/60'/0'/0/2",
			},
			Address: []string{
				"0xe729B863e83297458051b7C4E1515107614F0F8d",
				"0x6acF4e47067a8aa2306d897eB47046c91E89B329",
				"0x72034f320683656354F1AE85786B10cf80E9dB81",
			},
			PublicKey: []string{
				"0x033789e8d3f2db57b8592a51306cf054f24df18a59fdb19edbc756b0ce3b08f540",
				"0x03599ea72298336fc75c65aa30b6f387d589c7d7467b9092479b44590317c6980d",
				"0x03bfdf6f46d2bd8a6e1c93fc0915707c61835e1e00e511dd5e46cba02245569f7f",
			},
			PrivateKey: []string{
				"0xa7c91380e271569c87fc7f503ede7f2e75c6c719ecf3bc7830133ee2ed575fc7",
				"0xb3619440fe8212dddad225fe2739a4a40122de90e5ad00d4f00df174e4ddc1a8",
				"0x3ad5308fac3f765df6e8a4bd2b0fc691007298498c7cf76c2634b36d277937cc",
			},
		},
		{
			BitSize:     160,
			Mnemonic:    "spirit local then slender harvest wait govern inflict chef tail word cram",
			Pass:        "123",
			CoinType:    CoinTypeLTC,
			Seed:        "43c4d719d017f98290203da7627bb6bd49fee87cec48c61b38c8216d31a6e969198c222dc8c0f700dfaac129948438c7860cd1d6e0b76667f282c5f16372918f",
			RootKey:     "Ltpv71G8qDifUiNetVskyyMJBsU2pU4jpmNR1BBmGwX67A4vGuCoyt1poZWfRWRiv75Jw7FxRrDMnTCN6nAuqTjh2uERyAjh2JuYtp7ssfbEbrj",
			AccountPKey: "Ltpv77sWxR2W3817kFXkrz9bHCxLbpkPomfT8TT1UX1bRcr6oQBgEbx6ZKXjff7qDhwE64FoahgQUQuqJ7WfmCrA5CcU2qjTvRVQ4VukAjYiH5C",
			Path: []string{
				"m/44'/2'/0'/0/0",
				"m/44'/2'/0'/0/1",
				"m/44'/2'/0'/0/2",
			},
			Address: []string{
				"LbTMihpSDpVd4ZCNi59ZBqNjBx9qHfbV1p",
				"Ld2Diy1Ffjr6CS6eqhesBvW12MJXzMhLUs",
				"LeuqYxJw6MdzgRbhB1gVot1XryZs1owHon",
			},
			PublicKey: []string{
				"0212ea955bdc7797edea2323844a18b77c5cf329ff00f7311b6755c372edb3b64d",
				"02278d382dafa424a4dd46e63e612a1058ee4df47bee37bb090f57877041150f95",
				"02641671dc3af7b5b538a0b3cfe8d1d9cff8032418b944520c3260d26820fdfa40",
			},
			PrivateKey: []string{
				"T9ynSTJMCZnG2Z9L8sSrCZJkD2nJTxNL1DeZhjRzi2JcgLckYwb3",
				"T4ajdhpZJKfcULAXyYDuGm5wn2zSpCVevgiXEZLesvx8uuVmFXEL",
				"T947CQBkTZPvfuDmkLSyELHwXydVxErH43K4D8k8GK5UXKgZGySV",
			},
		},
		{
			BitSize:     160,
			Mnemonic:    "spirit local then slender harvest wait govern inflict chef tail word cram",
			Pass:        "123",
			CoinType:    CoinTypeBCH,
			Seed:        "43c4d719d017f98290203da7627bb6bd49fee87cec48c61b38c8216d31a6e969198c222dc8c0f700dfaac129948438c7860cd1d6e0b76667f282c5f16372918f",
			RootKey:     "Ltpv71G8qDifUiNetVskyyMJBsU2pU4jpmNR1BBmGwX67A4vGuCoyt1poZWfRWRiv75Jw7FxRrDMnTCN6nAuqTjh2uERyAjh2JuYtp7ssfbEbrj",
			AccountPKey: "Ltpv77sWxR2W3817kFXkrz9bHCxLbpkPomfT8TT1UX1bRcr6oQBgEbx6ZKXjff7qDhwE64FoahgQUQuqJ7WfmCrA5CcU2qjTvRVQ4VukAjYiH5C",
			Path: []string{
				"m/44'/145'/0'/0/0",
				"m/44'/145'/0'/0/1",
				"m/44'/145'/0'/0/2",
			},
			Address: []string{
				"qp6yrfh89u032f8l87ff8dkvur7dnz08evull2uq0h",
				"qzzl84e524hmfduvkpvf42hz3vjqv59kwu5ph24zfe",
				"qrzvmjlpt3uc6xrvejunmdm67yks2n7ddce9wzmxk0",
			},
			PublicKey: []string{
				"02679b5b1d53352fb2541ae759ed9f6002ee2b586f48b41717e6b1bd6cd1937227",
				"03c1eae72e7c5406c823c36d47fb6c5abb0b881ee60369b99e11f97cf1bcae7892",
				"02337c52a9828301f14943bb05f69c5b84fd139be536961eecdd2193daf42e3bac",
			},
			PrivateKey: []string{
				"L4umrPrdcdvf2kWh5H5z2Vgyt7r12ui8fKtfWejjC8i96WCqZSqo",
				"KzRJ2C3Faw4UjFebQ6VNAhgq3KH4qrrNtVERD65qcudigNqNKB8T",
				"KxBkhGbR3cFyrhbna3A6BjQpHvYghfwgHJeiF8Gb6pe7gdrCyp9N",
			},
		},
		{
			BitSize:     160,
			Mnemonic:    "spirit local then slender harvest wait govern inflict chef tail word cram",
			Pass:        "123",
			CoinType:    CoinTypeDASH,
			Seed:        "43c4d719d017f98290203da7627bb6bd49fee87cec48c61b38c8216d31a6e969198c222dc8c0f700dfaac129948438c7860cd1d6e0b76667f282c5f16372918f",
			RootKey:     "Ltpv71G8qDifUiNetVskyyMJBsU2pU4jpmNR1BBmGwX67A4vGuCoyt1poZWfRWRiv75Jw7FxRrDMnTCN6nAuqTjh2uERyAjh2JuYtp7ssfbEbrj",
			AccountPKey: "Ltpv77sWxR2W3817kFXkrz9bHCxLbpkPomfT8TT1UX1bRcr6oQBgEbx6ZKXjff7qDhwE64FoahgQUQuqJ7WfmCrA5CcU2qjTvRVQ4VukAjYiH5C",
			Path: []string{
				"m/44'/5'/0'/0/0",
				"m/44'/5'/0'/0/1",
				"m/44'/5'/0'/0/2",
			},
			Address: []string{
				"XgoUMfU1ty5TKfQYT8tg2sbwgZHPffQJtd",
				"Xt8hx4Sx9tdQjMMv93vxMNK6ydu5FruWVG",
				"XfZWdPxbSvZ9Chr8pvQnjcEcLbWsRzXJ6d",
			},
			PublicKey: []string{
				"02d7e8bc557cc68fb5a79f1e23781cbb34090f4fd703634a99a70b42845c6eec4f",
				"029a29643b01d7f184b984b1bda6285cf5c58b4836fec80a04532106d3cf1a89f9",
				"038f08e43f61a620ebf2810c071e7046c6db8a55f87d225af4b0af16245abb17dc",
			},
			PrivateKey: []string{
				"XJccioRwKxGNnADeDA9X9iz7gbr1sqr9hqBv56XReBZDFLnJDaq5",
				"XG4X9q7PvtH5rjGTqxuoPKWxvG1GS983f4fZRshYpse9PXoy1W4d",
				"XG38pmHV2g6hpCUgMkU5e7hYd4S7E2UXWPBx6T8XQYnpU49iKMkp",
			},
		},
		{
			BitSize:     160,
			Mnemonic:    "spirit local then slender harvest wait govern inflict chef tail word cram",
			Pass:        "",
			CoinType:    CoinTypeXRP,
			Seed:        "43c4d719d017f98290203da7627bb6bd49fee87cec48c61b38c8216d31a6e969198c222dc8c0f700dfaac129948438c7860cd1d6e0b76667f282c5f16372918f",
			RootKey:     "xprv9s21ZrQH143K3XEBY5Jg3TFHdCbsjmh5bCADKT8f7WUqG6Rw7FFGSip1Tjti3PS81zggMLZqdNVi7H3qSLkiPns61bnWiMrX6RVypCRDCxv",
			AccountPKey: "xpub6D9TpfhjELmSDje2RnXbJiaHPzoH7AtR3kYS66LBHKYPw1UjvZPkLNKFwewKiVha7YDRvL1Mcuy5F5MY5Nc7s25oxJSaakwVtcJ2CnY9eva",
			Path: []string{
				"m/44'/144'/0'/0/0",
				"m/44'/144'/0'/0/1",
				"m/44'/144'/0'/0/2",
			},
			Address: []string{
				"rFf4Sxdg3tw4X8ohW5PDrYKX79qinaC6o",
				"rKAFWJvwanyqoCKvGAP48CFezzhewhciyt",
				"r4RuUJnaRHMnFiu9GR3v5vfPH2Lr2uQcZj",
			},
			PublicKey: []string{
				"02bc1c61498377f70392d7855d74432338f5392324d2bdd11ac8488f416ceadf98",
				"020cf0d4613254476937aa9c3881cc9645032d819b5b0f5a3c72d184d8237dbe26",
				"0286d9717e5746c0beb2b03cc98b60042e8e6f9830088ad3a2b4c531c27b0a4108",
			},
			PrivateKey: []string{
				"8ae4c45cff2c2676d4bd52c3b093ddbbd1e1b7af37b1bd89e503facaec559a80",
				"4d36c5153da12ffed75997b0fd62ef5ce1a226851bc262b7cc5cb77761d6026d",
				"bf09bb359f1f5dc9fac77c9c02c97bad952129eaf990a4fdd86e4d142392d3aa",
			},
		},
	}
	for i := 6; i < 7; i++ {
		datum := data[i]
		testGenAddr(t, datum.BitSize, datum.Mnemonic, datum.Seed, datum.CoinType, datum.RootKey, datum.AccountPKey, datum.Path, datum.Address, datum.PublicKey, datum.PrivateKey)
	}
}

func testGenAddr(t *testing.T, BitSize int, mnemonic string, seed string, coinType CoinType, rootKey string, accountKey string, path []string, address []string, publicKey []string, privateKey []string) {
	km, err := NewKeyManager(BitSize, "123", mnemonic)
	if err != nil {
		t.Fatal(err)
	}
	passphrase := km.GetPassphrase()
	if passphrase == "" {
		passphrase = "<none>"
	}
	assert.Equal(t, mnemonic, km.GetMnemonic())
	assert.Equal(t, "123", km.GetPassphrase())
	assert.Equal(t, seed, hex.EncodeToString(km.GetSeed()))
	//masterKey, err := km.GetMasterKey()
	//assert.NoError(t, err)
	//assert.Equal(t, rootKey, masterKey.bip32Key.String())

	//accoKey, err := km.GetAccountKey(PurposeBIP44, coinType, 0)
	//assert.NoError(t, err)
	//assert.Equal(t, accountKey, accoKey.bip32Key.String())

	for i := 0; i < len(path); i++ {
		key, err := km.GetKey(PurposeBIP44, coinType, 0, 0, uint32(i))
		if err != nil {
			t.Error(err)
		}
		assert.Equal(t, path[i], key.GetPath())
	}

	for i := 0; i < len(address); i++ {
		key, err := km.GetKey(PurposeBIP44, coinType, 0, 0, uint32(i))
		if err != nil {
			t.Error(err)
		}
		addr, err := key.Address()
		if err != nil {
			t.Error(err)
		}
		assert.Equal(t, address[i], addr)
		//t.Logf("%-18s %-34s %s\n", key.GetPath(), address, wif)
	}
	for i := 0; i < len(publicKey); i++ {
		key, err := km.GetKey(PurposeBIP44, coinType, 0, 0, uint32(i))
		if err != nil {
			t.Error(err)
		}
		addr := key.PublicKey()
		assert.Equal(t, publicKey[i], addr)
		//t.Logf("%-18s %-34s %s\n", key.GetPath(), address, wif)
	}
	for i := 0; i < len(privateKey); i++ {
		key, err := km.GetKey(PurposeBIP44, coinType, 0, 0, uint32(i))
		if err != nil {
			t.Error(err)
		}
		addr, err := key.PrivateKey()
		assert.NoError(t, err)
		assert.Equal(t, privateKey[i], addr)
		//t.Logf("%-18s %-34s %s\n", key.GetPath(), address, wif)
	}
}
