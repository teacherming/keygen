package dogecoin

import (
	"github.com/btcsuite/btcd/chaincfg"
)

var MainNetParams = chaincfg.MainNetParams

func init() {
	MainNetParams.PubKeyHashAddrID = 0x1e // 30
	MainNetParams.ScriptHashAddrID = 0x16 // 22
	MainNetParams.PrivateKeyID = 0x9e     // 158
	MainNetParams.HDCoinType = 0
}
