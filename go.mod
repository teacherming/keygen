module gitee.com/teacherming/keygen

go 1.14

require (
	github.com/btcsuite/btcd v0.23.4
	github.com/btcsuite/btcd/btcec/v2 v2.2.1
	github.com/btcsuite/btcd/btcutil v1.1.2
	github.com/dashpay/dashd-go v0.24.0
	github.com/dashpay/dashd-go/btcutil v1.2.0
	github.com/decred/dcrd/dcrec/secp256k1/v4 v4.1.0 // indirect
	github.com/ethereum/go-ethereum v1.10.23
	github.com/fbsobreira/gotron-sdk v0.0.0-20210409100018-9213b0ef130f
	github.com/gcash/bchd v0.17.2-0.20201218180520-5708823e0e99
	github.com/gcash/bchutil v0.0.0-20210113190856-6ea28dff4000
	github.com/kr/pretty v0.3.0 // indirect
	github.com/ltcsuite/ltcd v0.22.0-beta
	github.com/ltcsuite/ltcd/btcec/v2 v2.1.0
	github.com/ltcsuite/ltcd/ltcutil v1.1.0
	github.com/onsi/ginkgo v1.16.5
	github.com/onsi/gomega v1.17.0
	github.com/renproject/pack v0.2.5
	github.com/stretchr/testify v1.7.2
	github.com/tyler-smith/go-bip32 v1.0.0
	github.com/tyler-smith/go-bip39 v1.1.0
	golang.org/x/crypto v0.0.0-20220214200702-86341886e292
)
